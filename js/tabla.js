document.getElementById("mostrarTabla").addEventListener("click", function () {
    const numeroTabla = parseInt(document.getElementById("selectTabla").value);
    const tablaResultado = document.getElementById("tablaResultado");
    tablaResultado.innerHTML = "";

    if (numeroTabla >= 1 && numeroTabla <= 10) {
        for (let i = 1; i <= 10; i++) {
            const multiplicacion = numeroTabla * i;
            const fila = document.createElement("div");
            fila.classList.add("fila");

            const elemento1 = createImageElement(numeroTabla + ".png", numeroTabla);
            const elemento2 = createImageElement("x.png", "x");
            const elemento3 = createImageElement(i + ".png", i);
            const elemento4 = createImageElement("=.png", "=");
            const resultadoStr = multiplicacion.toString();
            if (resultadoStr.length === 1) {
                const elemento5 = createImageElement("0.png", "0");
                const elemento6 = createImageElement(resultadoStr + ".png", resultadoStr);
                fila.appendChild(elemento1);
                fila.appendChild(elemento2);
                fila.appendChild(elemento3);
                fila.appendChild(elemento4);
                fila.appendChild(elemento5);
                fila.appendChild(elemento6);
            } else {
                const elemento5 = createImageElement(resultadoStr.charAt(0) + ".png", resultadoStr.charAt(0));
                const elemento6 = createImageElement(resultadoStr.charAt(1) + ".png", resultadoStr.charAt(1));
                fila.appendChild(elemento1);
                fila.appendChild(elemento2);
                fila.appendChild(elemento3);
                fila.appendChild(elemento4);
                fila.appendChild(elemento5);
                fila.appendChild(elemento6);
            }

            tablaResultado.appendChild(fila);
        }
    } else {
        tablaResultado.innerHTML = "Por favor, selecciona una tabla válida (del 1 al 10).";
    }
});

function createImageElement(src, alt) {
    const img = document.createElement("img");
    img.src = src;
    img.alt = alt;
    return img;
}
