const paises = [
{
"pais": "Estados Unidos",
"moneda": "Dólar estadounidense",
"valor_cambio": 1.00
},
{
"pais": "España",
"moneda": "Euro",
"valor_cambio": 0.85
},
{
"pais": "Japón",
"moneda": "Yen",
"valor_cambio": 110.62
},
{
"pais": "Reino Unido",
"moneda": "Libra esterlina",
"valor_cambio": 0.74
},
{
"pais": "Canadá",
"moneda": "Dólar canadiense",
"valor_cambio": 1.28
},
{
"pais": "Australia",
"moneda": "Dólar australiano",
"valor_cambio": 1.36
},
{
"pais": "China",
"moneda": "Yuan",
"valor_cambio": 6.37
},
{
"pais": "India",
"moneda": "Rupia india",
"valor_cambio": 74.92
},
{
"pais": "Brasil",
"moneda": "Real brasileño",
"valor_cambio": 5.35
},
{
"pais": "México",
"moneda": "Peso mexicano",
"valor_cambio": 20.17
},
{
"pais": "Alemania",
"moneda": "Euro",
"valor_cambio": 0.85
},
{
"pais": "Francia",
"moneda": "Euro",
"valor_cambio": 0.85
},
{
"pais": "Italia",
"moneda": "Euro",
"valor_cambio": 0.85
},
{
"pais": "Rusia",
"moneda": "Rublo ruso",
"valor_cambio": 80.31
},
{
"pais": "Sudáfrica",
"moneda": "Rand",
"valor_cambio": 14.88
}
]

const rangoPaisesInput = document.getElementById("rangoPaises");
const nombrePais = document.getElementById("nombrePais");
const tipoMoneda = document.getElementById("tipoMoneda");
const valorCambio = document.getElementById("valorCambio");

rangoPaisesInput.addEventListener("input", function () {
    const cantidad = parseInt(rangoPaisesInput.value);
    const pais = paises[cantidad - 1]; 

    if (pais) {
        nombrePais.value = pais.pais;
        tipoMoneda.value = pais.moneda;
        valorCambio.value = pais.valor_cambio;
    } else {
        nombrePais.value = "";
        tipoMoneda.value = "";
        valorCambio.value = "";
    }
});
